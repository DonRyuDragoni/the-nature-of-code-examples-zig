const PVector = @import("vector.zig").PVector;

usingnamespace @import("c.zig");

// If these look familiar to you, don't worry, it's not a déjà vu. They indeed
// are the ball_radius and ball_color from before, though they do have a
// slightly different meaning here, so they need different names. You'll
// understand shy in a few lines.

const default_radius = 48;

const default_color = SDL_Color {
    .r = 255,
    .g = 255,
    .b = 255,
    .a = 255,
};

pub const Ball = struct {
    // This bit wasn't on the PVector file. "@This()" simply means the innermost
    // type (in this case, Ball). This means that if we wish to change the name
    // of this struct in the future, we'd only need to change it in one place in
    // this whole file.
    //
    // Don't worry if it seems that this serves a very specific purpose, you'll
    // see fancier uses of this in the future.
    const Self = @This();

    // If these look familiar to you, don't worry, it's not a déjà vu. Any
    // Ball-related thigs from the main file were moved to here, only with two
    // minor changes: the radius and the color are now variables inside of a
    // Ball, instead of being global constants.
    //
    // So, if you decide to make three balls, each in a unique color and with
    // different sizes, you can just change these properties for each of them
    // instead of creating more constants.

    position: PVector,
    velocity: PVector,

    radius: i16,

    color: SDL_Color,

    // Much like we did for PVectors, here's a convenient function to create a
    // Ball.
    //
    // However, instead of requiring all properties to be passed to the
    // initialization function, some of them are initialized with default
    // values, and that's why we needed those constants. You could, with little
    // effect on readability, move those values into here, but some may find
    // that this style is a bit more elegant.
    pub fn init(position: PVector, velocity: PVector) Self {
        return Self {
            .position = position,
            .velocity = velocity,

            .radius = default_radius,

            .color = default_color,
        };
    }

    // Like init, you'll see various functions with this name, and for a good
    // reason: it is useful to separete what should change on every frame of
    // animation from the drawing logic.
    //
    // As you may notice, the first argument to this function is a star-Self
    // (or, more precise, a reference to a Self). This means this self is the
    // real current Self, and not a copy of a Self (which is what happens when
    // you don't star a type in a function argument), which means that the
    // changes in here will affect the Self that called it, rather than a copy
    // of it.
    //
    // Also, since the size of the window isn't a property of the Ball, and we
    // need those values to know when to flip its speed, they need be required
    // as arguments. (I'm still taking them as c_ints just so I can hide their
    // conversion in this function, as it is the only place they'll need to be
    // f32s.)
    //
    // You may ask why isn't the window size a property of the Ball, and my best
    // answer to that is philosophy. What defines a Ball? For example, would you
    // expect to find the paper size of the book you're reading an important
    // part of the story, or just the medium in which the story is being told?
    //
    // To look at another perspective, what would happen if we allowed the user
    // to change the size of the window? Each Ball would copy the window size on
    // initialization, but that value would get outdated on the first change, so
    // it would be our responsability to keep it up-to-date on every frame of
    // the animation, and we'd probably end up writing a function to do it. So,
    // if these values are only relevant here, why go through all this trouble?
    // I personally think it isn't worth it in this cenario, but that doesn't
    // mean it never will. Experience and practice will tell you when it is.
    pub fn update(self: *Self, win_width: c_int, win_height: c_int) void {
        self.position.add(self.velocity);

        const x_i32 = @floatToInt(i32, self.position.x);
        const y_i32 = @floatToInt(i32, self.position.y);

        if(x_i32 + self.radius > win_width or x_i32 - self.radius < 0) {
            self.velocity.x = self.velocity.x * -1;
        }

        if(y_i32 + self.radius > win_height or y_i32 - self.radius < 0) {
            self.velocity.y = self.velocity.y * -1;
        }
    }

    // Like init and update, this is also a common name, now guarding the logic
    // of drawing a ball to the renderer. You may notice a big difference from
    // update in the signature of this function: since Self shouldn't be changed
    // in here, it is marked as a const. Why not make a copy then? Well, we
    // don't really need a copy here either, do we? And copies can be noticeably
    // expensive, depending on the size of your struct, so it's a good idea to
    // use references whenever possible.
    //
    // (The renderer, on the other hand, cannot be marked as const, since we're
    // modifying it by drawing our circle.)
    //
    // And why isn't the renderer also a property of the Ball? Philosophy, I
    // guess. And the same arguments from before.
    pub fn display(self: *const Self, renderer: *SDL_Renderer) void {
        _ = filledCircleRGBA(
            renderer,
            @floatToInt(i16, self.position.x),
            @floatToInt(i16, self.position.y),
            self.radius,
            self.color.r, self.color.g, self.color.b, self.color.a
        );
    }
};
