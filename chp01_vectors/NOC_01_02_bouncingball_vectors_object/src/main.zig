// Here's our shiny new toy for this example: structures. Have a look inside it,
// and be sure to identify the bits of code that got moved into this new file.
const Ball = @import("ball.zig").Ball;

// Also be sure to check out how we're doing PVectors. If you're having trouble
// understanding this code, check the comented version on the previous example.
const PVector = @import("vector.zig").PVector;

usingnamespace @import("c.zig");

const background = SDL_Color {
    .r = 0,
    .g = 0,
    .b = 0,
    .a = 255,
};

const initial_position = PVector.init(
    100,
    100,
);
const initial_velocity = PVector.init(
    0.0025,
    0.005,
);

const width: c_int = 800;
const height: c_int = 200;

pub fn main() anyerror!void {
    _ = SDL_Init(SDL_INIT_EVERYTHING);
    defer SDL_Quit();

    const window = SDL_CreateWindow(
        c"Bouncing ball (vectors and objects)",
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        width, height,
        SDL_WINDOW_SHOWN,
    ) orelse unreachable;
    defer SDL_DestroyWindow(window);

    const renderer = SDL_CreateRenderer(
        window, -1, 0,
    ) orelse unreachable;
    defer SDL_DestroyRenderer(renderer);

    var done = false;

    // Much like our PVectors, we're initializing our Ball, except we're not
    // doing it with floats, but with PVectors.
    var ball = Ball.init(initial_position, initial_velocity);

    while(!done) {
        var event: SDL_Event = undefined;

        while(SDL_PollEvent(&event) != 0) {
            switch(event.type) {
                SDL_QUIT => done = true,

                SDL_KEYDOWN => {
                    if(event.key.keysym.sym == SDLK_ESCAPE) {
                        done = true;
                    }
                },

                else => {}
            }
        }

        // Now the code is starting to look much cleaner, doesn't it? Can you
        // think of a way to abstract the main loop and the event loop?
        ball.update(width, height);

        // These are still not part of the Ball's drawing logic, since all we're
        // doing is repainting the canvas in the background color.
        _ = SDL_SetRenderDrawColor(
            renderer,
            background.r,
            background.g,
            background.b,
            background.a,
        );

        _ = SDL_RenderClear(renderer);

        ball.display(renderer);

        // Again, not a part of the Ball's drawing logic. Think of it this way:
        // what if we had multiple Balls instead of just one, and we want all of
        // them present in every frame of animation. Which one should change the
        // canvas? And does it make sense to have a Ball changing the canvas?
        // What if we add now a Paddle (yes, pong!), which one should handle it?
        //
        // The easiest answer here is to say that none of them should do it, and
        // let this piece of code be simply a part of the main loop.
        _ = SDL_RenderPresent(renderer);
    }
}
