// For this example, all we really need for a PVector "class" is addition and
// the ability to hold two floating-point numbers.
//
// While we could define the whole PVector class by checking its documentation
// in Processing, I think the best approach would be to slowly grow it as needed
// for the examples. This incremental approach also means that you'll have less
// to read and more time to understand each piece.

// A Pvector is simply a public structure.
//
// "Public" here means that this struct is visible to the the world outside this
// file, while "struct" means that this isn't a simple variable (like an integer
// or a floating-point number), but something capable of holding some values (in
// this case, two f32s) and manipulating them. Which values and what kinds of
// manipulations? Well, I'm glad you asked, because that's what we're defining
// next.
pub const PVector = struct {
    // We want this vector to hold two values, x and y, and they're both
    // floating-point numbers.
    x: f32,
    y: f32,

    // This is kind of stretching the definition of manipulation, but creation
    // can be a kind of manipulation, right?
    //
    // This function exists mainly because it simplifies PVector creation.
    // Without it, we'd need to do the same thing we've been doing for
    // SDL_Color:
    //
    //     const position = PVector {
    //         .x = 100,
    //         .y = 100,
    //     };
    //
    // while with it, we can simply write:
    //
    //     const position = PVector.init(100, 100);
    //
    // Even though it hides a bit of what is going on, the second option looks
    // much cleaner, doesn't it?
    pub fn init(x: f32, y: f32) PVector {
        return PVector {
            .x = x,
            .y = y,
        };
    }

    // This one, however, is a proper manipulation, and it is how we're going to
    // add two vectors.
    //
    // Zig doesn't support what's called operator overloading, meaning we can't
    // define addition as
    //
    //     position + speed
    //
    // instead, we'll need a special function that does this. The next best
    // thing, then, is to have
    //
    //     position.add(speed)
    //
    // and, fortunately, we can define that, so here it is.
    pub fn add(self: *PVector, other: PVector) void {
        self.x = self.x + other.x;
        self.y = self.y + other.y;
    }
};
