pub usingnamespace @cImport({
    @cInclude("SDL2/SDL.h");
    @cInclude("SDL2_gfxPrimitives.h");
});

pub fn SDL_WINDOWPOS_CENTERED_DISPLAY(x: c_uint) c_uint {
    return SDL_WINDOWPOS_CENTERED_MASK | x;
}

pub const SDL_WINDOWPOS_CENTERED = SDL_WINDOWPOS_CENTERED_DISPLAY(0);

pub const SDL_INIT_EVERYTHING =
    SDL_INIT_TIMER
    | SDL_INIT_AUDIO
    | SDL_INIT_VIDEO
    | SDL_INIT_EVENTS
    | SDL_INIT_JOYSTICK
    | SDL_INIT_HAPTIC
    | SDL_INIT_GAMECONTROLLER;
