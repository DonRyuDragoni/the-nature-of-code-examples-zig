// I'm assuming that you've read and (at least) grasped the key concepts of the
// previous exercise, so all that stuff will not be commented again.
//
// If you find all that stuff about SDL confusing, don't worry too much about
// it, but do make an effort to understand a little bit of what's going on. If
// you need to, take a peek at the SDL docs for the function you're having
// trouble with, they can be found here:
//
//     https://wiki.libsdl.org/CategoryAPI
//
// Unless, of course, that function belongs to the SDL_gfx extension, in which
// case you should look in it's documentation, located here:
//
//     http://www.ferzkopp.net/Software/SDL_gfx-2.0/Docs/html/index.html

// To begin with, unlike Processing, Zig doesn't have a Vector class, so we'll
// have to create it from scratch. You're free to ignore this code for now, but
// you're also free to open the hood and take a peek at the motor driving this
// program.
const PVector = @import("vector.zig").PVector;

usingnamespace @import("c.zig");

const background = SDL_Color {
    .r = 0,
    .g = 0,
    .b = 0,
    .a = 255,
};

const ball_color = SDL_Color {
    .r = 255,
    .g = 255,
    .b = 255,
    .a = 255,
};

const ball_rad: i16 = 48;

// This is pretty much the same as the previous example, but notice how the
// PVector better represents those concepts with fewer variables.
//
// Instead of separating the x and y components of each of these, we can have
// both in the same place, just slightly hidden behind a PVector.
const initial_position = PVector.init(
    100, // This is the x component of the position.
    100, // And this is the y component.
);
const initial_velocity = PVector.init(
    0.0025, // Again, x component...
    0.005,  // ... and y component.
);

// Yes, I could also represent this with a PVector, but I like having the names
// "width" and "height" around.
//
// Also, a PVector is a vector of floats, and these need to be integers most of
// the time, so we'd have to make more conversions then it's worth at this
// point.
const width: c_int = 800;
const height: c_int = 200;

pub fn main() anyerror!void {
    _ = SDL_Init(SDL_INIT_EVERYTHING);
    defer SDL_Quit();

    const window = SDL_CreateWindow(
        c"Bouncing ball (vectors)",
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        width, height,
        SDL_WINDOW_SHOWN,
    ) orelse unreachable;
    defer SDL_DestroyWindow(window);

    const renderer = SDL_CreateRenderer(
        window, -1, 0,
    ) orelse unreachable;
    defer SDL_DestroyRenderer(renderer);

    var done = false;

    // Just like before, we create variables to represent the current position
    // and speed of the ball, but now they're grouped in vectors. While
    // initialization looks the same, addition will change a little bit.
    var position = initial_position;
    var velocity = initial_velocity;

    while(!done) {
        var event: SDL_Event = undefined;

        while(SDL_PollEvent(&event) != 0) {
            switch(event.type) {
                SDL_QUIT => done = true,

                SDL_KEYDOWN => {
                    if(event.key.keysym.sym == SDLK_ESCAPE) {
                        done = true;
                    }
                },

                else => {}
            }
        }

        // Now that we've switched to using vectors, we cannot simply write
        //
        //     position + velocity
        //
        // since Zig wouldn't know how to apply "+" to two PVectors (this is
        // called operator overloading, and Zig doesn't support it). Instead, we
        // need to call a function inside position that does that for us. The
        // syntax is a bit different, but the idea's the same: add the speed to
        // the position.
        position.add(velocity);

        // This awkward conversion still needs to be done. And, since it happens
        // more than once, it makes sense to have them as variables.
        const x_i32 = @floatToInt(i32, position.x);
        const y_i32 = @floatToInt(i32, position.y);

        if(x_i32 + ball_rad > width or x_i32 - ball_rad < 0) {
            velocity.x = velocity.x * -1;
        }

        if(y_i32 + ball_rad > height or y_i32 - ball_rad < 0) {
            velocity.y = velocity.y * -1;
        }

        _ = SDL_SetRenderDrawColor(
            renderer,
            background.r,
            background.g,
            background.b,
            background.a,
        );

        _ = SDL_RenderClear(renderer);

        _ = filledCircleRGBA(
            renderer,
            @floatToInt(i16, position.x), @floatToInt(i16, position.y),
            ball_rad,
            ball_color.r, ball_color.g, ball_color.b, ball_color.a
        );

        _ = SDL_RenderPresent(renderer);
    }
}
