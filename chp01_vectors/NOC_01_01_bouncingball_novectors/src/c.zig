// Import the needed C libraries. and expose everything inside them to the
// current namespace.
//
// If you don't know, this just means that stuff like SDL_Init will be defined
// as `c.SDL_Init()` instead of something like `c.sdl.SDL_Init()`.
pub usingnamespace @cImport({
    @cInclude("SDL2/SDL.h");
    @cInclude("SDL2_gfxPrimitives.h");
});

// These are nice things to have, but Zig cannot parse the macros, so we'll have
// to redifine them.

// Position a window in the center of the given display.
pub fn SDL_WINDOWPOS_CENTERED_DISPLAY(x: c_uint) c_uint {
    return SDL_WINDOWPOS_CENTERED_MASK | x;
}

// Position a window in the center of the first display it finds.
pub const SDL_WINDOWPOS_CENTERED = SDL_WINDOWPOS_CENTERED_DISPLAY(0);

// Initialize all SDL subsystems.
pub const SDL_INIT_EVERYTHING =
    SDL_INIT_TIMER
    | SDL_INIT_AUDIO
    | SDL_INIT_VIDEO
    | SDL_INIT_EVENTS
    | SDL_INIT_JOYSTICK
    | SDL_INIT_HAPTIC
    | SDL_INIT_GAMECONTROLLER;
