// First, we need to import all that C code to use SDL.
usingnamespace @import("c.zig");

// Second, some constants.

// Defining some colors just so the code is a bit more readable.

// You can think of this one as a layer of paint we're putting before we even
// start to draw.
const background = SDL_Color {
    .r = 0,
    .g = 0,
    .b = 0,
    .a = 255,
};

// And this is the color that will be used for drawing the ball.
const ball_color = SDL_Color {
    .r = 255,
    .g = 255,
    .b = 255,
    .a = 255,
};

// Speaking of the ball, here are its radius, initial position and speed. Adjust
// to taste.
const ball_rad: i16 = 48;

const initial_x: f32 = 100;
const initial_y: f32 = 100;

const initial_x_speed: f32 = 0.005;
const initial_y_speed: f32 = 0.001;

// You could query the window for its current size, but doing this and making
// the window unresizable is much easier.
const width: c_int = 800;
const height: c_int = 200;

// Third, (if you didn't already know this) we have to define the entry point of
// our program. Until now we've only being defining things, this is where all
// those pieces interact.
//
// And yes, the entry point is just another public function.
pub fn main() anyerror!void {
    // Initialize all subsystems in SDL. For this program, this means that, from
    // now on, we can create the window.
    _ = SDL_Init(SDL_INIT_EVERYTHING);

    // We initiated SDL, so we need to quit it. The `defer` means that this will
    // only be executed when this function returns, even though it looks like
    // we're calling it now.
    defer SDL_Quit();

    // To show something on the screen, we need to create one of those floaty
    // thingies (or, as SDL boringly calls them, windows). Remember that its
    // size was defined as constans just above in the variables `width` and
    // `height`.
    const window = SDL_CreateWindow(
        c"Bouncing ball (no vectors)",
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        width, height,
        SDL_WINDOW_SHOWN,
    ) orelse unreachable;

    // We created a window, so, when this function returns, we're expected to
    // clean up our mess by destroying it.
    defer SDL_DestroyWindow(window);

    // Now, one cannot simply draw onto a window, we need a canvas that fits
    // inside it to place our beautiful circle. SDL calls it a renderer, though.
    const renderer = SDL_CreateRenderer(
        window, -1, 0,
    ) orelse unreachable;

    // We created a renderer, so, when this function returns, we're expected to
    // clean up our mess by destroying it. You got it this time? Good, now go
    // clean up your room. Or don't, I'm just a comment in a code, not your
    // mother.
    defer SDL_DestroyRenderer(renderer);

    // We're surely not done yet.
    var done = false;

    // The properties of our ball: its current location and speed. They'll be
    // changing constantly, so they cannot be called `const`s.
    var x: f32 = initial_x;
    var y: f32 = initial_y;
    var x_speed: f32 = initial_x_speed;
    var y_speed: f32 = initial_y_speed;

    // The main loop of our program. This is here to make our code continue to
    // execute after the first draw and until it thinks it is done.
    //
    // We're still not done, so let's continue.
    while(!done) {
        // Sometimes, you click on things, press buttons or type on a
        // keyboard. This variable will tell us if anything interesting
        // happened, but it isn't useful on its own, so may I present you...
        var event: SDL_Event = undefined;

        // THE EVENT LOOP.
        //
        // Which is just a fancy name for another loop that runs through all the
        // events the window revieved since the last time we drew something on
        // it.
        //
        // Not unlike a child, it keeps asking if we're done yet.
        while(SDL_PollEvent(&event) != 0) {
            // Now, our dear `event` is quite the little prodigy, being able to
            // store information on various types of events. In this case, we're
            // only interested in two of them.
            switch(event.type) {
                // User asked to close the window. This usually means clicking
                // on that little "x" button on the top of the window.
                //
                // And, yes, Michael, when the user clicks that "x" we'll be
                // done. But not before it.
                SDL_QUIT => done = true,

                // The user's cat pressed random keys on the
                // keyboard. Fortunately, we're only interested in one of them,
                // and little Furball didn't reach it.
                SDL_KEYDOWN => {
                    // If the user pressed the escape key, we'll be done too.
                    //
                    // I'm putting this here only because it's more convenient
                    // than taking my hand off the keyboard to reach for the
                    // mouse.
                    if(event.key.keysym.sym == SDLK_ESCAPE) {
                        done = true;
                    }
                },

                // All the other events may be ignored. This program is not
                // interactive at all.
                else => {}
            }
        }

        // We promised we'd be moving that ball, so here it is. The position of
        // the ball changes according to its speed.
        x = x + x_speed;
        y = y + y_speed;

        // This is just a Zig thing. We'll need x and y as integers and right
        // now they're floating-point numbers, so this is how we convert them.
        const x_i32 = @floatToInt(i32, x);
        const y_i32 = @floatToInt(i32, y);

        // One of the reasons is here: in Zig, one cannot simply compare an
        // integer to a floating-point number.
        //
        // This checks if the ball is hitting any of the walls in the x
        // direction; if it is, we invert its speed to simulate a collision.
        if(x_i32 + ball_rad > width or x_i32 - ball_rad < 0) {
            x_speed = x_speed * -1;
        }

        // Same thing, now for the y direction.
        //
        // Why are we using the radius of the ball here, you ask? Because x and
        // y represents the center of the ball. Without taking the radius into
        // account, the ball would bounce only when its middle hits the
        // wall. Totally unrealistic.
        if(y_i32 + ball_rad > height or y_i32 - ball_rad < 0) {
            y_speed = y_speed * -1;
        }

        // Remember back then when we defined the background color of our canvas?
        _ = SDL_SetRenderDrawColor(
            renderer,
            background.r,
            background.g,
            background.b,
            background.a,
        );

        // Clear the previous drawing by putting a layer of that background
        // paint on the canvas.
        //
        // Movies are a lie, we cannot really create movement on a computer. But
        // we sure can fake it if we draw fast enough. And computers are pretty
        // fast these days.
        _ = SDL_RenderClear(renderer);

        // After all this code, here it is, the drawing of our beautiful ball.
        _ = filledCircleRGBA(
            renderer,
            @floatToInt(i16, x), @floatToInt(i16, y),
            ball_rad,
            ball_color.r, ball_color.g, ball_color.b, ball_color.a
        );

        // You were lied again, we weren't drawing on the canvas of the window,
        // but on a second canvas, hidden behind the balcony. Here we're
        // swapping the two so we can show our most recent drawing while we
        // prepare to remake the one that was previously being appreciated by
        // the public
        _ = SDL_RenderPresent(renderer);
    } // If we're not done yet, do it all again!

    // Remember those times we said to clean stuff? Normally, that happens here.
    //
    // I mean, we're ignoring all the errors in this example, so things could've
    // gone wrong and we wouldn't know how to deal with them. But at the very
    // least the room would be sparkling clean regardless.
}
