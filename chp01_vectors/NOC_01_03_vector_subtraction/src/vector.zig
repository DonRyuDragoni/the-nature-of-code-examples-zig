pub const PVector = struct {
    const Self = @This();

    x: f32,
    y: f32,

    pub fn init(x: f32, y: f32) Self {
        return Self {
            .x = x,
            .y = y,
        };
    }

    pub fn add(self: *Self, other: Self) void {
        self.x = self.x + other.x;
        self.y = self.y + other.y;
    }

    pub fn sub(self: *Self, other: Self) void {
        self.x = self.x - other.x;
        self.y = self.y - other.y;
    }
};
