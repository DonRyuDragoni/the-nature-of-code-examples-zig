// Remember how I asked you to imagine an abstraction for the main and event
// loops last time? Here's one way to do it. Notice how pretty much all the code
// was moved into it, but everything has it's own little block.
const State = @import("state.zig").State;

// Since all we really need here is SDL_Init, SDL_Quit, SDL_INIT_EVERYTHING and
// our shiny SdlError, we may as well keep the namespace clean and import all
// those things under c.
//
// Be sure to check SdlError in here!
const c = @import("c.zig");

pub fn main() anyerror!void {
    // You could ceartainly justify moving SDL_Init and SDL_Quit into the
    // State. Where would you put them and why?
    //
    // Also, from now on we'll sort of be handling errors instead of just
    // ignoring them.  According to SDL's documentation, SDL_Init returns zero
    // on success and negative values on error, so the check is quite simple if
    // we don't care to know specifically what went wrong (if you do, take a
    // look at SDL's documentation for SDL_Init and see what it has to say
    // about it).
    if(c.SDL_Init(c.SDL_INIT_EVERYTHING) != 0) {
        return c.SdlError.Init;
    }

    // If SDL fails to initialize, we're returning with an error, and there's
    // nothing to cleanup; otherwise, we'll need to deinitialize it before
    // exiting our program. Proper manners and all that.
    defer c.SDL_Quit();

    // Wow, where did all that code go?
    var state = try State.init(800, 600); // That try over there means that the
                                          // the function may return an error
                                          // and, if it does, we'd like to pass
                                          // it on to whoever called main. So,
                                          // yes, we're making all this error
                                          // handling just so we can know what
                                          // went wrong if the program crashes
                                          // because of our code.

    // State has a deinit function, so we should probably call it on exit.
    defer state.deinit();

    state.loop();
}
