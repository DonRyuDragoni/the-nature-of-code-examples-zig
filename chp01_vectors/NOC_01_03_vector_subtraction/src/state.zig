// Just as before, we still need our C libraries.
usingnamespace @import("c.zig");

const PVector = @import("vector.zig").PVector;

// Instead of having global variables, we're now going to put everything we need
// inside a State struct.
pub const State = struct {
    const Self = @This();

    // Tell the state when to quit. This is just like before, only "done" is now
    // a member of State instead of a variable inside the main function.
    done: bool,

    // It's nice to have these names around, and now we don't need to keep them
    // as global variables.
    width:  c_int,
    height: c_int,

    // Since the logic to handle events was moved into this struct, it makes
    // sense that it should also handle the window. The same applies to the
    // render, only considering the drawing logic.
    window:   *SDL_Window,
    renderer: *SDL_Renderer,

    // This time, instead of having the background as a global constant, it'll
    // be a member of our State.
    background: SDL_Color,

    // In the same style as we did before, we have a function to conveniently
    // initialize our struct.
    //
    // The main thing to note in the function signature is how it doesn't
    // return a Self, but an SdlError!Self, which means either a Self or an
    // SdlError (which you can inspect by looking at c.zig). This is all part
    // of proper error handling in Zig.
    pub fn init(width: c_int, height: c_int) SdlError!Self {
        // As a sort of side note, you could use anyerror!Self (or !Self) to
        // mean all errors that may occur, not only those SDL-related. The fact
        // that we're not saying that means we're promising to handle those
        // errors here, and only passing the responsability of dealing with
        // those caused by SDL to the one who called this function (we got
        // here from the main function, remember?).

        const window = SDL_CreateWindow(
            c"Vector subtraction",
            SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
            width, height,
            SDL_WINDOW_SHOWN,
        ) orelse return SdlError.CreateWindow; // See how it isn't unreachable
                                               // anymore? We promised to
                                               // return an SdlError if things
                                               // went wrong, and that's what
                                               // we're doing here.

        // We do not want to destroy the window when this function returns,
        // but, if something goes wrong, we do want to do it, and that's where
        // errdefer comes into play. It is pretty much a defer, except it only
        // gets executed on error returns. So, from now on, if anything goes
        // wrong, the window will be destroyed before returning from this
        // function with an error.
        errdefer SDL_DestroyWindow(window);

        // Now we do the same thing for the renderer.
        const renderer = SDL_CreateRenderer(
            window, -1, 0,
        ) orelse return SdlError.CreateRenderer;

        // You may argue that this is useless, since, if the renderer gets
        // created nothing else can go wrong in this function, but I'd say it's
        // a good practice to always put it so you never forget about it.
        //
        // Also, consider that this wasn't an example, but a real-world
        // program, one that grows more complex with time. You may need to add
        // code after this point, and that code may fail and return an error.
        // It is then much easier to forget that this renderer would never be
        // cleaned up then, and just keep adding more code. So get used to
        // deferring (err or vanilla) whenever it may be needed.
        errdefer SDL_DestroyRenderer(renderer);

        return Self {
            .done = false,

            .width  = width,
            .height = height,

            .window   = window,
            .renderer = renderer,

            .background = SDL_Color {
                .r = 0,
                .g = 0,
                .b = 0,
                .a = 255,
            },
        };
    }

    // This is new, isn't it?
    //
    // Unlike our Ball and our PVector, this struct initializes some things
    // that need to be destroyed when the State itself is destroyed, so this
    // function is supposed to handle that.
    //
    // Why create another function instead of deferring these thins in init?
    // Well, because defer only works on the current scope, meaning that, if
    // we'd done that, the renderer and the window would've been destroyed in
    // init itself, which is not ideal if we still need them, right? Instead,
    // we want them to exist as long as the State that created them does, so we
    // create a function that should be called when the State is about to
    // finish its thing (or, to put in more technical terms, to go out of
    // scope).
    //
    // As for the name, it could be called anything, really. Zig's standard
    // library calls it deinit, though, so we probably should do the same for
    // convention's sake.
    //
    // And how would you know if a struct needs to do some cleaning or not? If
    // you're using someone else's code, they should tell you that in their
    // documentation; otherwise, experience will build your intuition for these
    // things.
    pub fn deinit(self: *Self) void {
        // Be sure to remember that things should, in general, be destroyed in
        // reverse order of creation. For this example, it means destroying the
        // renderer before the window.
        SDL_DestroyRenderer(self.renderer);
        SDL_DestroyWindow(self.window);
    }

    // Also new is the way we're defining the main loop of our program, and this
    // is it. Notice how simple it gets once we separate its pieces into
    // different functions?
    pub fn loop(self: *Self) void {
        while(!self.done) {
            self.handle_events();
            self.render();
        }
    }

    // This is also a candidate for refactoring. Much like what was done to the
    // main loop, the event loop could be separated into different components
    // for each of the events. However, considering how simple this program is,
    // refactoring this code may be a case of over-engineering. One could argue
    // that refactoring the main function already is, but this is also ment to
    // be educational, so some over-engineering should be acceptable, right?
    fn handle_events(self: *Self) void {
        var event: SDL_Event = undefined;

        while(SDL_PollEvent(&event) != 0) {
            switch(event.type) {
                SDL_QUIT => self.done = true,

                SDL_KEYDOWN => {
                    if(event.key.keysym.sym == SDLK_ESCAPE) {
                        self.done = true;
                    }
                },

                else => {}
            }
        }
    }

    // And the last piece of the puzzle: the drawing logic.
    fn render(self: *const Self) void {
        _ = SDL_SetRenderDrawColor(
            self.renderer,
            self.background.r,
            self.background.g,
            self.background.b,
            self.background.a,
        );

        _ = SDL_RenderClear(self.renderer);

        // Now here's a way to get the mouse position. It only works if the
        // mouse is in the window, though.
        var mouse_x: c_int = undefined;
        var mouse_y: c_int = undefined;
        _ = SDL_GetMouseState(&mouse_x, &mouse_y);

        // We're here to demonstrate vector subtraction, so let's do it. First,
        // we need to convert our mouse into one:
        var mouse = PVector.init(
            @intToFloat(f32, mouse_x),
            @intToFloat(f32, mouse_y),
        );

        // And our canvas size:
        const center = PVector.init(
            @intToFloat(f32, self.width) / 2,
            @intToFloat(f32, self.height) / 2,
        );

        // Now, as the example did, lets take the difference between the mouse
        // position and the center of the canvas.
        mouse.sub(center);

        // We want to draw a line from the center of the canvas (lets call it
        // point_a) to the mouse position (point_b), where the center of the
        // canvas scorresponds to the point (0, 0).
        var point_a = PVector.init(0, 0);
        var point_b = mouse;

        // However, now we have a problem: SDL doesn't have (as far as I can
        // tell) an equivalent of "translate" to change the origin point of the
        // window, so we have to move point_a and point_b accordingly.
        point_a.add(center);
        point_b.add(center);

        // So, yes, we just demonstrated vector subtraction by subtracting and
        // then adding the same value to a vector, essentially doing
        // nothing. Plase blame SDL, not me, for this.

        // Setting the drawing color...
        _ = SDL_SetRenderDrawColor(self.renderer, 255, 255, 255, 255);

        // ... and drawing the line from the two calculated points.
        _ = SDL_RenderDrawLine(
            self.renderer,
            @floatToInt(c_int, point_a.x), @floatToInt(c_int, point_a.y),
            @floatToInt(c_int, point_b.x), @floatToInt(c_int, point_b.y),
        );

        _ = SDL_RenderPresent(self.renderer);
    }
};
