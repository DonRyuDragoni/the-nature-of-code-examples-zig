# The Nature Of Code: Examples Zig

This is a port of the examples presented in The Nature Of Code [book][noc book],
organized in the same way as [their GitHub repository][noc repo]. This version
uses the [Zig programming language][] (especifically, zig-0.5.0), [SDL2][] for
the graphics and Box2D for the physics.

Each example is expected to be self-contained, so, aside from SDL itself, all
code needed to run each project is in its folder. This does lead to a lot of
code duplication, but I believe clarity is worth the sacrifice.

Depending only on C libraries and not using a nice wrapper like Processing means
this code may not be for beginners, but I believe in you! (I'll also try to
comment every relevant new thing, so hopefully you won't feel to lost in all
this C.)

[noc book]: https://natureofcode.com/
[noc repo]: https://github.com/nature-of-code/noc-examples-processing
[Zig programming language]: https://ziglang.org/
[SDL2]: https://www.libsdl.org/
